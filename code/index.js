const date = `© 2002 – ${new Date().getFullYear()} Мережа піцерій Domino!`,
    validate = (r, v) => r.test(v);

const user = {
    name: "",
    phone: "",
    email: ""
}


window.addEventListener("DOMContentLoaded", () => {
    document.getElementById("address").innerText = date;
    const form = document.getElementById("contact-form");
    const [...inputsForm] = document.querySelectorAll("#contact-form input");

    function inputValidate() {
        debugger
        let item = this;

        if (item.type === "text" && validate(/^[А-яіїґє-]+$/i, item.value)) {
            user.name = item.value.toLowerCase();
            item.classList.add("success");
            item.classList.remove("error")
        } else if (item.type === "email" && validate(/^[A-z_0-9.]+@[A-z-]+\.[A-z]{1,4}\.?[A-z]*$/, item.value)) {
            user.email = item.value.toLowerCase();
            item.classList.add("success");
            item.classList.remove("error")
        } else if (item.type === "tel" && validate(/^\+380[0-9]{9}$/, item.value)) {
            user.phone = item.value.toLowerCase();
            item.classList.add("success");
            item.classList.remove("error")
        }
        else {
            item.classList.add("error");
            item.classList.remove("success")
        }
    }

    inputsForm.forEach((item) => {
        if (item.type === "text" || item.type === "email" || item.type === "tel") {
            item.addEventListener("change", inputValidate)
        }
    })

    form.addEventListener("submit", (e) => {
        console.log("contact-form");

        inputsForm.forEach(function (item) {

            if (item.type === "text" || item.type === "email" || item.type === "tel") {
                inputValidate.apply(item)
            }
        })
        e.preventDefault()
    })
})


